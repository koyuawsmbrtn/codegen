# Codegen

Command-line tool to generate confirmation codes for koyu.space

## Installation

You'll need Python 3 and the dependencies from the `requirements.txt` file. Then start the script and follow the instructions from the setup wizard.
